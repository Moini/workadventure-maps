## Workadventure maps testing area

This is where I play around with my own little workadventure map.

When I have a link to my map, and I'm 'home', you can visit me there. 

### License: 
The tiles all come with their own license, please check the corresponding directory.
